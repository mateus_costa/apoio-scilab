/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testescilab;
import org.scilab.modules.javasci.Scilab;
import org.scilab.modules.types.ScilabDouble;
import org.scilab.modules.types.ScilabType;

/**
 *
 * @author Thanner Soares Silva <thannersoares@gmail.com>
 */
public class Testescilab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        MenuPrincipal mp = new MenuPrincipal();
        mp.run();
        System.out.println("oi");
        try {
            System.out.println("oi");

            Scilab sci = new Scilab();
            System.out.println("oi");

            sci.open();
            System.out.println("oi");

            sci.exec("disp(%pi);");
            System.out.println("oi");

            ScilabDouble a = new ScilabDouble(3.14);
            sci.put("a", a);
            System.out.println("oi");
            sci.exec("b=sin(a);");
            ScilabType b = sci.get("b");
            System.out.println("b = " + b);

            sci.close();

        } catch (org.scilab.modules.javasci.JavasciException e) {
            System.err.println("An exception occurred: " + e.getLocalizedMessage());
        }

    }

}
